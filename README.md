# Docker Compose for NodeJS

This Docker Compose template is meant to run a NodeJS application on Linux, shipped with some useful developer tools.

## Index

1. [How to use this Docker Compose project](#how-to-use-this-docker-compose-project)
    - [Docker stack](#docker-stack)
    - [Requirements](#requirements)
    - [Installation](#installation)
2. [License](#license)
3. [About](#about)


## How to use this Docker Compose project

### Docker stack

* Node 18 + MariaDB, powered by Debian 11 "Bullseye"


### Requirements

- [Linux OS](https://en.wikipedia.org/wiki/List_of_Linux_distributions)
- [Docker](https://docs.docker.com/get-docker/)
- [Docker Compose](https://docs.docker.com/compose/install/)


### Installation

1. Clone this project:
   ```
   git clone https://gitlab.com/deviktta/docker-compose-for-nodejs.git .docker-compose-for-nodejs && \
   cd .docker-compose-for-nodejs
   ```

2. Create the Docker environment file with the provided default and edit it with your project values:
   ```
   cp default.env .env
   ```
   
3. Run the Docker containers:
   ```
   docker-compose up -d --build
   ```
   
4. Update directories' permissions:
   ```
   sudo chmod +x ./bin/ -R
   ```

**Available tools (see executables in `./bin/`)**
* NPM: `./bin/npm`
* NPX: `./bin/npx`
* Yarn: `./bin/yarn`

Application logs can be found in `./logs` directory.


## License
This project is under [MIT License](/LICENSE.txt).


## About
Contact e-mail: [deviktta@protonmail.com](mailto:deviktta@protonmail.com); made with love from Barcelona, Catalunya.
